import React from 'react';
import './timer.css';

interface Props {}

interface State {
	days: number,
	hours: number,
	min: number,
	sec: number,
	time: number,
	timerId: number
}

class Timer extends React.Component<Props, State> {

	constructor(props: Props) {
		super(props)
		this.state = {
			time: 189543,
			days: 0,
			hours: 0,
			min: 0,
			sec: 0,
			timerId: 0,
		}
	}

	updateTime = () =>{
		const {time: current_time} = this.state
		if(current_time === 0)
			clearInterval(this.state.timerId);

		let days =  Math.floor(current_time / 24 / 60 / 60);
		let hour_left = Math.floor((current_time) - (days * 86400));
		let hours = Math.floor(hour_left / 3600);
		let min_left = Math.floor((hour_left) - (hours * 3600));
		let min = Math.floor(min_left / 60);
		let remSeconds = current_time % 60;

		this.setState({
			days,
			hours,
			min,
			sec: remSeconds,
			time: current_time - 1
		})
	}

	resetTimer = () => {
		this.setState({
			time: 189543
		})
	}

	componentDidMount = () => {
		const timerId : number = window.setInterval(this.updateTime, 1000);
		this.setState({timerId})
	}

	componentWillUnmount = () => {
		clearInterval(this.state.timerId)
	}

	render() {
		const {days, hours, min, sec} = this.state;
		return (
			<div className="timer">
				<div className="heading"> Timer </div>
				<div className="main">
					{days ? (<div className="main-quantity">{days + " days"}</div>) : null}
					{hours ? (<div className="main-quantity">{hours + " hours"}</div>) : null}
					{min ? (<div className="main-quantity">{min + " minutes"}</div>) : null}
					{sec ? (<div className="main-quantity">{sec + " seconds"}</div>) : null}
				</div>
				<div className="reset-buttons">
					<div className="reset-button" onClick={() => {this.resetTimer()}}> Reset </div>
				</div>
			</div>
		)
	}
}

export default Timer;